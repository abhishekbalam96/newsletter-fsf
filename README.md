# New codebase for Newsletter with SQLITE3 Db.

Note: This app uses Python 2.7.

Instructions:
- For Testing in Development env:
	1. Enter Virtual Environment:
		- `source venv/bin/activate`
	2. Install python dependencies:
		- `pip install -r requirements.txt`
	3. Initalize Sqlite3 database:
		- For first use, uncomment `flask init-db` command in `dev.sh`
		- Creates a `newsletter.sqlite3` file in `instance/` with tables defined in `schema.sql`
		- **PLS COMMENT AFTER 1st TIME** as it creates a fresh database. **All data will be lost otherwise**.
	4. Setup Nginx to proxy `port 5000`
	5. To run dev server:
		- `sh dev.sh` 
		- This is temp. Will change later.

- For Production:
	1. Setup Nginx to proxy `port 5000`
	2. Review `install.sh`. It does the following:
		- build a docker image
		- runs it in disconnected mode
		- initialises a fresh sqlite db `instance/newsletter.sqlite3`.
	3. Run `sh install.sh`
	4. Test the site

NOTE: Inside the container the app runs on `port 8000`
Next:
-  Open Browser at: [http://127.0.0.1:5000/ ](http://127.0.0.1:5000/)
-  `/users/all` gives a json response with all users

TODO:
1. ~~Email Template for confirmation.~~
2. ~~Making a Docker Image for this.~~
3. ~~Scout for bugs.~~
4. ~~Refine Blueprint System.~~