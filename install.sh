#! /bin/bash

# Build Image
docker build -t newsletter:latest .

# Run Image
docker run -d -p 5000:8000 newsletter:latest

# Utils
# docker ps -aq # list
# docker stop $(docker ps -aq) # stop all
# docker rm $(docker ps -aq) # rm all cont.
# docker rmi $(docker images -q) # rm all images


# To Initalise Sqlite DB
IMG_NAME=`docker inspect --format='{{.Name}}' $(docker ps -aq --no-trunc) | cut -c2-`
echo $IMAGE_NAME
docker exec -it $IMG_NAME flask init-db

echo "Docker Container Setup successfully.\n The app is running in Port 5000"