#! /bin/sh 

export FLASK_APP=newsletter
export FLASK_ENV=development
# I used this to perform commands from urls. 
# For example https://newsletter.abhishekbalam.xyz/publish/fumero
# will publish my latest newsletter
export APP_TOKEN=fumero 

export mail_password=zHxj9htEcHmT  # Pls keep this confidential.
export mail_username=newsletter@abhishekbalam.xyz
export SITE_URL="http://localhost:5000"
export SECRET_KEY=dev

### DB Commands
## For fresh DB Initilization only
flask init-db  

## Syncing users and verified
# flask sync-verified

## Backup sqlite db to anothe file.
# flask backup-db


### For running the application
## Development Server
# flask run

## Production Server
gunicorn -w 4 --bind 0.0.0.0:8000 newsletter.wsgi:app